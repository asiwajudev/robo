export const robots = [
  {
    id: 1,
    name: "John Doe",
    username: "doe",
    email: "johndoe@gmail.com"
  },
  {
    id: 2,
    name: "John Bow",
    username: "bow",
    email: "johnbow@gmail.com"
  },
  {
    id: 3,
    name: "Paul Doe",
    username: "Paul",
    email: "paulddoe@gmail.com"
  },
  {
    id: 4,
    name: "John Paul",
    username: "johnp",
    email: "johnp@gmail.com"
  },
  {
    id: 5,
    name: "John Downey",
    username: "downey",
    email: "johndowney@gmail.com"
  },
  {
    id: 6,
    name: "John Doppy",
    username: "doppyu",
    email: "johndoppy@gmail.com"
  },
  {
    id: 7,
    name: "John Penny",
    username: "penny",
    email: "johnpenny@gmail.com"
  },
  {
    id: 8,
    name: "John Grup",
    username: "grup",
    email: "johngrup@gmail.com"
  },
  {
    id: 9,
    name: "John Potty",
    username: "Pot",
    email: "johnpot@gmail.com"
  },
  {
    id: 10,
    name: "John Potry",
    username: "Potry",
    email: "johnpotry@gmail.com"
  }
];
