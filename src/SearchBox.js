import React from "react";

const SeacrhBox = ({ searchField, searchChange }) => {
  return (
    <div className="pa2">
      <input
        type="Search"
        className="pa3 ba b--green bg-lightest-blue"
        placeholder="Search Robots"
        onChange={searchChange}
      />
    </div>
  );
};

export default SeacrhBox;
