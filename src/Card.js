import React from "react";
import "./Card.css";

//Card Component
//Receives the information from robots.js into an array, then destructure to filter the needed details
const Card = ({ name, email, id }) => {
  return (
    <div className="bg-light-green dib br3 pa3 ma2 grow bw2 shadow-5 tc card">
      <img src={`https://robohash.org/${id}`} id="robo-image" alt="robot" />
      <div>
        <h2>{name}</h2>
        <p>{email}</p>
      </div>
    </div>
  );
};

export default Card;
