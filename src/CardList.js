import React from "react";
import Card from "./Card";

//this function is to render the card component multiple times
const CardList = ({ robots }) => {
  //receives info from robot.js file and map/loop as cardArra
  //renders the card
  return (
    <div>
      {robots.map((user, i) => {
        return (
          <Card
            key={i}
            id={robots[i].id}
            name={robots[i].name}
            email={robots[i].email}
          />
        );
      })}
    </div>
  );
};

export default CardList;
